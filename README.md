Chatify where you can see and talk about what everyone's listening to.
It runs inside Spotify for easy to access to what you and your fellow chatters are jamming to.

To install:

1. Sign up for a Spotify developer account if you haven't already
    - https://developer.spotify.com/technologies/apps/#developer-account

2. Clone repository or download [the zip](https://github.com/imcnally/chatify/archive/master.zip) to ~/Spotify/

3. In the Spotify search bar type: spotify:app:chatify
