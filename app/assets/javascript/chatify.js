(function(angular, io, getSpotifyApi) {

  var SERVER = "http://chatify-server.herokuapp.com";
  // var SERVER = "http://localhost:5000";
  var app = angular.module('Chatify', []);
  var socket = io.connect(SERVER);
  var sp = getSpotifyApi();
  var models = sp.require("sp://import/scripts/api/models");
  var player = models.player;
  var username = models.session.anonymousUserID.slice(0, 5) || 'anonymous-user';

  app.controller('MessagesController', function($scope) {

    $scope.messages = [];

    socket.on('newMessage', function(message) {
      $scope.messages.push(message);
      $scope.$digest();
    });

  });

  app.controller('SendController', function($scope, Message) {

    $scope.sendMessage = function() {
      socket.emit('messageSent', new Message($scope.messageEntry));
      $scope.messageEntry = '';
    };

  });

  app.controller('ListenersController', function($scope, Listener) {

    $scope.addTrackChangeEventListener = function() {
      player.observe(models.EVENT.CHANGE, function (e) {
        if (e.data.curtrack === true) {
          socket.emit('trackUpdate', new Listener(player.track))
        }
      });
    }

    $scope.addTrackChangeEventListener();

    $scope.listeners = [];

    socket.on('listenerUpdate', function(listener) {
      $scope.listeners = [listener].concat(_.filter($scope.listeners, function(l) {
        l.username !== listener.username;
      }));
      $scope.$digest();
    });

  });

  app.factory('Message', function() {
    return function(messageText) {
      return {
        text : messageText,
        username : username,
        time : new Date().getTime()
      };
    };
  });

  app.factory('Listener', function() {
    return function(track) {
      return {
        track : track.data.name,
        artist : track.album.artist.name,
        username : username
      };
    };
  });

  app.filter('date', function() {
    return function(input) {
      d = new Date(input);
      return d.getHours() + ":" + d.getMinutes();
    };
  });

})(angular, io, getSpotifyApi);